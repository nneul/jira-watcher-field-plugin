package com.burningcode.jira.plugin;

import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import com.opensymphony.module.propertyset.map.MapPropertySet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PropertySetManager.class)

public class TestWatcherFieldSettings {

	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(PropertySetManager.class);
		MapPropertySet propertySet = new MapPropertySet();
		propertySet.setMap(new HashMap<String, Object>());
		when(PropertySetManager.getInstance(eq("ofbiz"), anyMap())).thenReturn(propertySet);
	}

	protected void assertBooleanSetting(final String setting, final boolean defaultSetting) {
		PropertySet propertySet = WatcherFieldSettings.getPropertySet();
		assertTrue(propertySet.exists(setting));
		assertTrue(propertySet.getObject(setting) instanceof Boolean);
		assertTrue(propertySet.getBoolean(setting) == defaultSetting);

		propertySet.setBoolean(setting, !defaultSetting);
		propertySet = WatcherFieldSettings.getPropertySet();
		assertTrue(propertySet.getObject(setting) instanceof Boolean);
		assertTrue(propertySet.getBoolean(setting) == !defaultSetting);
	}

	@Test
	public void testStaticGetPropertySet_ignorePermissions() {
		assertBooleanSetting(WatcherFieldSettings.ignoreUserPermissions, false);
	}

	@Test
	public void testStaticGetPropertySet_ignoreWatcherPermissions() {
		assertBooleanSetting(WatcherFieldSettings.ignoreWatcherPermissions, false);
	}

	@Test
	public void testStaticGetPropertySet_ignoreDeactivatedWatchers() {
		assertBooleanSetting(WatcherFieldSettings.ignoreDeactivatedWatchers, false);
	}

	@Test
	public void testStaticGetPropertySet_automaticallyRemoveDeactivatedWatchers() {
		assertBooleanSetting(WatcherFieldSettings.automaticallyRemoveDeactivatedWatchers, false);
	}

	@Test
	public void testIsIgnoreUserPermissions() {
		PropertySet propertySet = WatcherFieldSettings.getPropertySet();
		propertySet.setBoolean(WatcherFieldSettings.ignoreUserPermissions, true);

		assertTrue(WatcherFieldSettings.isIgnoreUserPermissions(null));
	}

	@Test
	public void testIsIgnoreUserPermissions_withMissingIgnoreUserPermissionSetting() {
		PropertySet propertySet = WatcherFieldSettings.getPropertySet();
		propertySet.remove(WatcherFieldSettings.ignoreUserPermissions);

		assertFalse(WatcherFieldSettings.isIgnoreUserPermissions(null));
	}

	@Test
	public void testIsIgnoreUserPermissions_withIgnoreUserPermissionSettingFalse() {
		PropertySet propertySet = WatcherFieldSettings.getPropertySet();
		propertySet.setBoolean(WatcherFieldSettings.ignoreUserPermissions, false);

		assertFalse(WatcherFieldSettings.isIgnoreUserPermissions(null));
	}

	@Test
	public void testIsIgnoreUserPermissions_withNonNullUser() {
		ApplicationUser user = mock(ApplicationUser.class);
		PropertySet propertySet = WatcherFieldSettings.getPropertySet();
		propertySet.setBoolean(WatcherFieldSettings.ignoreUserPermissions, true);

		assertFalse(WatcherFieldSettings.isIgnoreUserPermissions(user));
	}
}
