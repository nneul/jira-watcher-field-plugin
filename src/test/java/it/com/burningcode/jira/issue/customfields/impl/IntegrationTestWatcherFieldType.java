
package it.com.burningcode.jira.issue.customfields.impl;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.*;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel;
import com.atlassian.jira.functest.framework.admin.IssueSecurityLevel.IssueSecurity;
import com.atlassian.jira.functest.framework.admin.IssueSecuritySchemes.IssueSecurityScheme;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.atlassian.jira.functest.framework.assertions.TextAssertions;
import com.atlassian.jira.functest.framework.locator.IdLocator;
import com.atlassian.jira.functest.framework.locator.Locator;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryItem;
import com.atlassian.jira.webtests.ztests.workflow.ExpectedChangeHistoryRecord;
import com.burningcode.jira.plugin.WatcherFieldSettings;
import com.meterware.httpunit.WebForm;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.functest.framework.FunctTestConstants.*;
import static it.com.burningcode.jira.IntegrationTestHelper.*;
import static org.junit.Assert.*;

/**
 * Class used for integration testing for the JIRA Watcher Field Plugin.
 *
 * TODO Get integration testing on bulk change.
 * TODO Check issue security
 * TODO Write test to check for issue JWFP-9
 * @author Ray Barham
 */
@SuppressWarnings("SameParameterValue")
@LoginAs(user = "admin")
public class IntegrationTestWatcherFieldType extends BaseJiraFuncTest {
	@Inject
	private Form form;

	@Inject
	public Administration administration;

	@Inject
	protected Assertions assertions;

	@Inject
	protected TextAssertions textAssertions;

	@Before
	public void setup() {
		// Seems to be a problem with the imported 'bob' user.  Deleting and re-adding fixes the issue until a new
		// import is created
        backdoor.usersAndGroups().deleteUser(BOB_USERNAME);
		backdoor.usersAndGroups().addUser(BOB_USERNAME, BOB_PASSWORD, BOB_FULLNAME, BOB_EMAIL);
	}

	private void assertIssueExists(String issueKey){
		String issueId = navigation.issue().getId(issueKey).trim();
		assertFalse("Issue doesn't exist.", issueId.isEmpty());
	}
    
    private void assertWatchersNotPresent(String issueKey, String[] watchers){
    	String currentPage = navigation.getCurrentPage();
    	assertIssueExists(issueKey);
    	navigation.issue().gotoIssue(issueKey);
    	
    	tester.clickLink("view-watcher-list");
    	for(String watcher : watchers){
    		tester.assertLinkNotPresent("watcher_link_" + watcher);
    	}
    	navigation.gotoPage(currentPage);
    }
    
    private void assertWatchersPresent(String issueKey, String[] watchers){
    	String currentPage = navigation.getCurrentPage();
    	navigation.issue().gotoIssue(issueKey);
    	
    	tester.clickLink("view-watcher-list");
    	for(String watcher : watchers){
    		tester.assertLinkPresent("watcher_link_" + watcher);
    	}
    	navigation.gotoPage(currentPage);
    }

	private void createWatcherField() {
		backdoor.customFields().createCustomField(FIELD_NAME, "", FIELD_TYPE_KEY, SEARCH_TYPE_KEY);
	}

	private String createIssue(String projectId, String type, String summary) {
        return createIssue(projectId, type, summary, new HashMap<>());
    }

	private String createIssue(String projectId, String type, String summary, Map<String, String[]> params) {
        gotoCreateIssueForm(projectId, type);
        tester.setFormElement("summary", summary);

		for (Object o : params.entrySet()) {
			Map.Entry entry = (Map.Entry) o;
			String[] value = (String[]) entry.getValue();
			tester.setFormElement((String) entry.getKey(), String.join(",", value));
		}
        tester.submit();

		return getIssueKey(new IdLocator(tester, "key-val"));
	}

	protected void editUsername(final String existingUsername, final String newUsername) {
		navigation.gotoPage("UserBrowser.jspa");
		tester.clickLink("edituser_link_" + existingUsername);

		tester.setFormElement("username", newUsername);
		tester.clickButton("user-edit-submit");
	}

    private void gotoCreateIssueForm() {
        gotoCreateIssueForm(PROJECT_ID, ISSUE_TYPE_BUG);
    }
    private void gotoCreateIssueForm(String projectId, String issueType) {
        navigation.gotoPage("CreateIssue!default.jspa");
        tester.setFormElement("pid", projectId);
        tester.setFormElement("issuetype", issueType);
        tester.submit("Next");
        tester.submit("Next");
    }

    private String getIssueKey(Locator locator) {
        return locator.getRawText();
    }
    
    private HashMap<String, String[]> getUsernameFieldMap(String[] usernames) {
    	HashMap<String, String[]> params = new HashMap<>();
    	params.put(FIELD_ID, usernames);
    	return params;
    }
    
    private void ignoreWatcherPermissions(boolean value){
    	navigation.gotoPage("/secure/project/EditWatcherFieldSettings!default.jspa");
        tester.setFormElement(WatcherFieldSettings.ignoreWatcherPermissions, String.valueOf(value));
        tester.submit("Update");
    }

    private void ignoreDeactivatedWatchers(boolean value){
    	navigation.gotoPage("/secure/project/EditWatcherFieldSettings!default.jspa");
        tester.setFormElement(WatcherFieldSettings.ignoreDeactivatedWatchers, String.valueOf(value));
        tester.submit("Update");
    }

	private void deleteDeactivatedWatchers(boolean value){
		navigation.gotoPage("/secure/project/EditWatcherFieldSettings!default.jspa");
		tester.setFormElement(WatcherFieldSettings.automaticallyRemoveDeactivatedWatchers, String.valueOf(value));
		tester.submit("Update");
	}

    private void moveIssue(String issueKey) {
        navigation.issue().gotoIssue(issueKey);
        tester.clickLinkWithText("Move");
        tester.setFormElement("pid", "Test Move");
        tester.clickButton("next_submit");
        tester.submit();
        tester.submit();
        tester.submit();
    }

    private void addUserToGroup(String username, String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.GROUP_BROWSER);
        tester.clickLink("edit_members_of_" + groupName);
        tester.setFormElement("usersToAssignStr", username);
        tester.submit("assign");
    }

    private void removeUserFromGroup(String username, String groupName) {
        navigation.gotoAdminSection(Navigation.AdminSection.USER_BROWSER);
        tester.clickLink("editgroups_" + username);
        tester.selectOption("groupsToLeave", groupName);
        tester.clickButton("user-edit-groups-leave");
    }

    private WebForm setWatcherFieldForm(WebForm[] forms, String values){
    	return setWatcherFieldForm(forms, values, null);
    }
    
    private WebForm setWatcherFieldForm(WebForm[] forms, String values, String expectedExistingValues){
    	// Loop through the forms till you one w/ the watcher field is found 
    	for(WebForm form : forms){	
    		if(form.hasParameterNamed(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID)){
    			if(expectedExistingValues != null){
    				tester.assertFormElementEquals(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID, expectedExistingValues);
    			}
    			form.setParameter(it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID, values);
    			return form;
    		}
    	}
    	fail("No form found with watcher field ID "+ it.com.burningcode.jira.IntegrationTestHelper.FIELD_ID);

    	return null;
    }
    
    /**
     * Test adding a watcher field to JIRA.
     */
	@Test
	@Restore(EXPORT_WITHOUT_FIELD)
    public void testCreateWatcherField() {
		navigation.gotoCustomFields();
		tester.assertTableNotPresent("custom-fields");

		createWatcherField();

		navigation.gotoCustomFields();
		tester.assertTextInTable("custom-fields", FIELD_TYPE);
    }
    
    /**
     * Test deleting a watcher field from JIRA.
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testDeleteWatcherField() {
    	navigation.gotoCustomFields();
    	tester.assertTextInTable("custom-fields", FIELD_NAME);
    	tester.assertTextInTable("custom-fields", FIELD_TYPE);
    	administration.customFields().removeCustomField(FIELD_ID);
    	
   		tester.assertTableNotPresent("custom-fields");
    }
    
    /**
     * Test adding watchers via the watcher field on issue create.
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testAddWatcherOnIssueCreate() {
    	HashMap<String, String[]> params = getUsernameFieldMap(new String[]{BOB_USERNAME});
		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create", params);
    	navigation.issue().gotoIssue(issueKey);
    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME});
    }

	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testWatchersEqualWatcherCounter() throws IOException, SAXException {
		administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
		addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);

		String[] watchers = new String[]{ADMIN_USERNAME, BOB_USERNAME, FRED_USERNAME};

		HashMap<String, String[]> params = getUsernameFieldMap(watchers);
		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create", params);

		Issue issue = backdoor.issues().getIssue(issueKey);
		assertEquals("Watcher counter does not equal number of watchers on the issue", watchers.length, issue.fields.watches.watchCount);

		navigation.issue().gotoEditIssue(issueKey);
		setWatcherFieldForm(form.getForms(), ADMIN_USERNAME + ", " + BOB_USERNAME).submit();

		issue = backdoor.issues().getIssue(issueKey);
		assertEquals("Watcher counter does not equal number of watchers on the issue", 2, issue.fields.watches.watchCount);
	}

    @Test
    @Restore(EXPORT_WITH_FIELD)
    public void testAutoWatcherDoesNotResetWatcherCounter() throws IOException, SAXException {
        administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);

        backdoor.userProfile().changeUserAutoWatch(true, ADMIN_USERNAME);

        String[] watchers = new String[]{BOB_USERNAME, FRED_USERNAME};

        HashMap<String, String[]> params = getUsernameFieldMap(watchers);
        String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test watcher does not reset count, #1", params);

        navigation.issue().gotoEditIssue(issueKey);

        assertWatchersPresent(issueKey, watchers);
        Issue issue = backdoor.issues().getIssue(issueKey);
		assertEquals("Watcher counter does not equal number of watchers on the issue", 3, issue.fields.watches.watchCount);

        watchers = new String[]{ADMIN_USERNAME, BOB_USERNAME, FRED_USERNAME};

		params = getUsernameFieldMap(watchers);
		issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test watcher does not reset count, #2", params);

		navigation.issue().gotoEditIssue(issueKey);

		assertWatchersPresent(issueKey, watchers);
		issue = backdoor.issues().getIssue(issueKey);
		assertEquals("Watcher counter does not equal number of watchers on the issue", watchers.length, issue.fields.watches.watchCount);
    }

    /**
     * Test adding watchers via the watcher field on issue edit.
     * @throws SAXException 
     * @throws IOException 
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testAddWatcherOnIssueEdit() throws IOException, SAXException {
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue edit.");
    	assertWatchersNotPresent(issueKey, usernames);
    	navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(form.getForms(), ADMIN_USERNAME + ", " + BOB_USERNAME).submit();
    	assertWatchersPresent(issueKey, usernames);
    }
    
    /**
     * Test modifying watchers via the watcher field.
     * @throws SAXException 
     * @throws IOException 
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testModifyingWatcherOnIssueEdit() throws IOException, SAXException {
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};
    	
    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test modify watchers on issue edit", params);

    	assertWatchersPresent(issueKey, usernames);
    	
    	navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(form.getForms(), usernames[0], usernames[0] + ", " + usernames[1]).submit();
    	
    	assertWatchersPresent(issueKey, new String[]{usernames[0]});
    	assertWatchersNotPresent(issueKey, new String[]{usernames[1]});
    }

    // TODO: Verify that this test is useful
	@Test
	@Restore("test_move.zip")
    public void testMovingIssueWithWatchers() throws IOException, SAXException {
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test move issue with watchers", params);

        removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);
        moveIssue(issueKey);
        assertIssueExists("TESTMOVE-1");
    }

	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testRenameWatcher() {
		HashMap<String, String[]> params = getUsernameFieldMap(new String[]{BOB_USERNAME});
		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test rename watchers", params);
		navigation.issue().gotoIssue(issueKey);
		assertWatchersPresent(issueKey, new String[]{BOB_USERNAME});

		final String newUsername = "newbob";

		editUsername(BOB_USERNAME, newUsername);

        navigation.issueNavigator().createSearch("\"My Watchers\"=" + BOB_USERNAME);
		textAssertions.assertTextNotPresent(issueKey);

        navigation.issueNavigator().createSearch("\"My Watchers\"=" + newUsername);
		textAssertions.assertTextPresent(issueKey);
	}

    /**
     * Test configuring the watcher field.
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testConfigureWatcherField() {
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test default watchers without configuration.");
    	assertWatchersNotPresent(issueKey, usernames);

    	// Set the default value for the watcher field
    	administration.customFields().setDefaultValue(NUMERIC_FIELD_ID, usernames[1]);

    	issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test default watchers with configuration.");
    	assertWatchersNotPresent(issueKey, new String[]{usernames[0]});
    	assertWatchersPresent(issueKey, new String[]{usernames[1]});
    }
    
    /**
     * Checks simple filter/searching using the watcher field.  Also checks that issues are being re-indexed on adding watchers (otherwise, searches would not work).
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testFilterByWatcher() throws Exception {
    	String[] usernames = new String[]{BOB_USERNAME};

    	HashMap<String, String[]> params = getUsernameFieldMap(usernames);
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test simple filter by watcher", params);
    	assertWatchersPresent(issueKey, usernames);

        navigation.issueNavigator().createSearch("\"My Watchers\"=" + BOB_USERNAME);
		textAssertions.assertTextPresent(issueKey);
    }

    /**
     * Checks that change history is effected properly.  See issue JWF-5.
     * @throws SAXException
     * @throws IOException
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testChangeHistory() throws IOException, SAXException {
		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test change history without watchers specified.");
		navigation.issue().gotoIssueChangeHistory(issueKey);

		navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(form.getForms(), BOB_USERNAME + ", " + ADMIN_USERNAME).submit();

		ExpectedChangeHistoryItem expectedChangeItem = new ExpectedChangeHistoryItem(FIELD_NAME, "None", "Administrator, Bob The Builder");
		ExpectedChangeHistoryRecord changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItem);

		assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);

		navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(form.getForms(), ADMIN_USERNAME).submit();

    	// Verify change history when changing watchers
        expectedChangeItem = new ExpectedChangeHistoryItem(FIELD_NAME, "Administrator, Bob The Builder", "Administrator");
        changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItem);

    	assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);

		navigation.issue().gotoEditIssue(issueKey);
    	setWatcherFieldForm(form.getForms(), "").submit();

    	// Verify change history when clearing watchers
        expectedChangeItem = new ExpectedChangeHistoryItem(FIELD_NAME, "Administrator", "None");
        changeHistoryRecord = new ExpectedChangeHistoryRecord(expectedChangeItem);

        assertions.assertLastChangeHistoryRecords(issueKey, changeHistoryRecord);
    }
    
    /**
     * Checks that watchers are edited properly on issue transition.  See issue JWF-4.
     * @throws SAXException 
     * @throws IOException 
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testEditWatcherOnIssueTransition() throws IOException, SAXException {
    	backdoor.screens().addFieldToScreen("Workflow Screen", FIELD_NAME);

    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test edit watchers on issue transition.");
    	navigation.issue().closeIssue(issueKey, "Fixed", null);
    	tester.clickLinkWithText("Reopen Issue");
    	setWatcherFieldForm(form.getForms(), BOB_USERNAME + ", " + ADMIN_USERNAME).submit();

    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME, ADMIN_USERNAME});
    }
    
    /**
     * Verifies that JWFP-13 is resolved
     * 
     * Run using: atlas-debug --jvmargs "-server -Xms1024m -Xmx1024m -XX:PermSize=256m -Datlassian.mail.senddisabled=false -Datlassian.mail.fetchdisabled=false -Datlassian.mail.popdisabled=false -Dmail.debug=true -Dmail.smtp.localhost=true"
     * 
     * @throws InterruptedException
     */
    /* Until services are able to be triggered manually, have to disable this test.
    @Test
	@Restore(EXPORT_WITH_FIELD)
	public void testCreateIssueViaEmail() throws InterruptedException, MessagingException, UnableToAddServiceException, UserException, FolderException {
    	log.log("### Test add watchers on create issue via email ###");

    	// Set the default user for the watcher field
    	administration.customFields().setDefaultValue(NUMERIC_FIELD_ID, BOB_USERNAME);
    	
		assertSendingMailIsEnabled();

		JIRAServerSetup.POP3.setPort(110);
		configureAndStartGreenMail(JIRAServerSetup.ALL);
		getGreenMail().setUser(ADMIN_EMAIL, ADMIN_USERNAME, ADMIN_PASSWORD);

		assertTrue(getGreenMail().getPop3().isAlive());
		assertTrue(getGreenMail().getSmtp().isAlive());
		assertTrue(getGreenMail().getImap().isAlive());
		
		// Setup the mail server in JIRA
		setupJiraImapPopServer();
		setupJiraMailServer(ADMIN_EMAIL, DEFAULT_SUBJECT_PREFIX, String.valueOf(getGreenMail().getSmtp().getPort()));

		// Add service to create issues from POP server
		setupPopService("project=" + PROJECT_KEY + ", issuetype=" + ISSUE_BUG);

        String subject = "This is created by email without watchers";
        String message = "This is the subject.  It is a test subject.";
        
        // Send the message
        GreenMailUtil.sendTextEmail(ADMIN_EMAIL, ADMIN_EMAIL, subject, message, getGreenMail().getSmtp().getServerSetup());
        
		waitForMail(1);
		Thread.sleep(65000);

        // Check that a default watcher was not added with the ignorePermissions set to false
        assertWatchersNotPresent(PROJECT_KEY + "-1", new String[]{BOB_USERNAME});

        // Set the ignorePermissions to true
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");
        tester.setFormElement("ignorePermissions", "true");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
        tester.submit("Update");
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");
        tester.assertRadioOptionSelected("ignorePermissions", "true");
        
        subject = "This is created by email with watchers";
        message = "This is the subject.  It is a test subject.";
        
        GreenMailUtil.sendTextEmail(ADMIN_EMAIL, ADMIN_EMAIL, subject, message, getGreenMail().getSmtp().getServerSetup());
		
		waitForMail(1);
		Thread.sleep(65000);

        // Check that a default watcher was added with the ignorePermissions set to true
        assertWatchersPresent(PROJECT_KEY + "-2", new String[]{BOB_USERNAME});
    }*/
    
    /*
    @Test
	@Restore(EXPORT_WITH_FIELD)
    public void testBulkEditWatchers() {
        Vector issueList = new Vector();
        issueList.add("TST-1");
        issueList.add("TST-2");
        
        // Bulk edit the watchers on some issues
        displayAllIssues();
        bulkChangeIncludeAllPages();
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        assertFormElementPresent("cbcustomfield_10000");
        selectCheckbox("cbcustomfield_10000");
        //setBulkEditFieldTo("customfield_"+FIELD_ID, "cbcustomfield_"+FIELD_ID);
        //bulkEditOperationDetailsSetAs(easyMapBuild("customfield_"+FIELD_ID, "bob"));
        clickOnNext();
        dumpScreen("screenDump");

        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementNotPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementNotPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");

        // Bulk edit the watchers on some issues
        displayAllIssues();
        clickLink("bulkedit_all");
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        setFormElement("customfield_"+FIELD_ID, "admin");
        clickButton("Next");
        clickButtonWithValue("Confirm");

        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementNotPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementNotPresent("stopwatch_bob");
        
        // Bulk edit the multiple watchers on some issues
        displayAllIssues();
        clickLink("bulkedit_all");
        bulkChangeSelectIssues(issueList);
        bulkChangeChooseOperationEdit();
        setFormElement("customfield_"+FIELD_ID, "admin, bob");
        clickButton("Next");
        clickButtonWithValue("Confirm");
        
        // Check that the users were actually added as watchers
        gotoIssue("TST-1");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
        gotoIssue("TST-2");
        clickLink("view_watchers");
        assertFormElementPresent("stopwatch_admin");
        assertFormElementPresent("stopwatch_bob");
    }
    */

	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testNonAdminUserManageWatchersWithoutPermission() {
		backdoor.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
    	
    	navigation.login(FRED_USERNAME, FRED_PASSWORD);

        gotoCreateIssueForm();

		textAssertions.assertTextPresent("You do not have permission to manage the watcher list.");
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create");
    	assertIssueExists(issueKey);
    }

	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testNonAdminUserManageWatchersWithPermission() {
    	administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);

		backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.MANAGE_WATCHERS, JIRA_DEV_GROUP);

    	navigation.login(FRED_USERNAME, FRED_PASSWORD);

        gotoCreateIssueForm();
    	assertions.getTextAssertions().assertTextNotPresent("You do not have permission to manage the watcher list.");
    	HashMap<String, String[]> params = getUsernameFieldMap(new String[]{BOB_USERNAME});
    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers on issue create", params);
    	assertIssueExists(issueKey);
    	assertWatchersPresent(issueKey, new String[]{BOB_USERNAME});
    }
    
    /**
     * Checks that JWFP-22 is resolved
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testWatcherFieldPermissions() {
    	ignoreWatcherPermissions(false);

        removeUserFromGroup(BOB_USERNAME, JIRA_USERS_GROUP);

        gotoCreateIssueForm();
    	tester.setFormElement("summary", "Test add watchers without permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();

    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

        tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();

        assertions.forms().assertNoErrorsPresent();

        assertWatchersPresent("TST-1", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-1", new String[]{BOB_USERNAME});
    	
    	ignoreWatcherPermissions(true);

        gotoCreateIssueForm();
    	tester.setFormElement("summary", "Test add watchers ignoring permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();

    	assertions.forms().assertNoErrorsPresent();

        assertWatchersPresent("TST-2", new String[]{BOB_USERNAME, ADMIN_USERNAME});
    	
    	ignoreWatcherPermissions(false);
    	
    	// Check that a user, when giving permission again, can be added as a watcher.
    	administration.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_USERS_GROUP);
        gotoCreateIssueForm();

    	tester.setFormElement("summary", "Test add watchers with permissions");
    	tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-3", new String[]{BOB_USERNAME, ADMIN_USERNAME});
    }

	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testDeactivatedUsersAsWatchersOnCreate() {
		ignoreDeactivatedWatchers(false);

		backdoor.userManager().setActive(BOB_USERNAME, false);

		gotoCreateIssueForm();

		tester.setFormElement("summary", "Test with ignoreDeactivatedWatchers disabled");
		tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
		tester.submit();

		assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

		tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
		tester.submit();

		assertions.forms().assertNoErrorsPresent();

		ignoreDeactivatedWatchers(true);

		gotoCreateIssueForm();

		tester.setFormElement("summary", "Test with ignoreDeactivatedWatchers enabled");
		tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
		tester.submit();

		assertions.forms().assertNoErrorsPresent();
	}

	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testDeactivatedUsersAsWatchersOnEdit() throws IOException, SAXException {
		ignoreDeactivatedWatchers(false);

		backdoor.userManager().setActive(BOB_USERNAME, false);

		String[] watchers = new String[]{ ADMIN_USERNAME };
		HashMap<String, String[]> params = getUsernameFieldMap(watchers);

		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test with ignoreDeactivatedWatchers disabled", params);

		navigation.issue().gotoEditIssue(issueKey);
		tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
		tester.submit();

		assertions.text().assertTextPresent("Users do not have permission to browse issue: " + BOB_USERNAME);

		ignoreDeactivatedWatchers(true);

		navigation.issue().gotoEditIssue(issueKey);
		tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
		tester.submit();

		assertions.forms().assertNoErrorsPresent();
	}

	@Test
	@Restore(EXPORT_WITH_FIELD)
	public void testAutomaticallyRemoveDeactivatedUsers() {
		deleteDeactivatedWatchers(false);

		String[] watchers = new String[]{ ADMIN_USERNAME, BOB_USERNAME };
		HashMap<String, String[]> params = getUsernameFieldMap(watchers);

		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test with automaticallyRemoveDeactivatedWatchers disabled", params);

		backdoor.userManager().setActive(BOB_USERNAME, false);

		navigation.issue().gotoEditIssue(issueKey);

		assertWatchersPresent(issueKey, watchers);

		deleteDeactivatedWatchers(true);

		navigation.issue().gotoEditIssue(issueKey);

		assertWatchersNotPresent(issueKey, new String[]{ BOB_USERNAME });
	}

//	@Test
//	@Restore(EXPORT_WITH_FIELD)
//	public void testAutomaticallyRemoveDeactivatedUsersWithoutPermission() {
//		deleteDeactivatedWatchers(false);
//
//		backdoor.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL);
//		backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
//
//		String[] watchers = new String[]{ ADMIN_USERNAME, BOB_USERNAME };
//		HashMap<String, String[]> params = getUsernameFieldMap(watchers);
//
//		String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test with automaticallyRemoveDeactivatedWatchers disabled", params);
//
//		backdoor.userManager().setActive(BOB_USERNAME, false);
//
//		navigation.issue().gotoEditIssue(issueKey);
//
//		assertWatchersPresent(issueKey, watchers);
//
//		deleteDeactivatedWatchers(true);
//
//		backdoor.permissionSchemes().removeProjectRolePermission(0, ProjectPermissions.MANAGE_WATCHERS, 10002);
//
//		navigation.login(FRED_USERNAME);
//
//		navigation.issue().gotoEditIssue(issueKey);
//
//		assertWatchersNotPresent(issueKey, new String[]{ BOB_USERNAME });
//	}

	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testWatcherFieldIssueSecurity() {
    	IssueSecurityScheme securityScheme = administration.issueSecuritySchemes().newScheme("Test Security", "");
    	IssueSecurityLevel securityLevel = securityScheme.newLevel("Restricted", "");
    	securityLevel.addIssueSecurity(IssueSecurity.GROUP, JIRA_ADMIN_GROUP);
		backdoor.permissionSchemes().addGroupPermission(DEFAULT_PERM_SCHEME_ID, ProjectPermissions.SET_ISSUE_SECURITY, JIRA_USERS_GROUP);

        tester.gotoPage("/plugins/servlet/project-config/" + PROJECT_KEY + "/issuesecurity");
        tester.clickLink("project-config-issuesecurity-scheme-change");
        tester.setWorkingForm(JIRA_FORM_NAME);
        tester.selectOption("newSchemeId", "Test Security");
        tester.submit("Next >>");
        tester.submit("Associate");

        gotoCreateIssueForm();
        tester.setFormElement("summary", "Test add watchers without issue security permissions");
        tester.setFormElement("security", "10000");
        tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
        tester.submit();

    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

    	tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-1", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-1", new String[]{BOB_USERNAME});
    	
    	ignoreWatcherPermissions(true);

        HashMap<String, String[]> params = new HashMap<>();
        params.put("security", new String[]{"10000"});
        params.put(FIELD_ID, new String[]{BOB_USERNAME, ADMIN_USERNAME});
        createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers ignoring permissions", params);

    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-2", new String[]{ADMIN_USERNAME, BOB_USERNAME});
    	
    	ignoreWatcherPermissions(false);

        gotoCreateIssueForm();
        tester.setFormElement("summary", "Test add watchers without issue security permissions");
        tester.setFormElement("security", "10000");
        tester.setFormElement(FIELD_ID, BOB_USERNAME + ", " + ADMIN_USERNAME);
        tester.submit();

    	assertions.forms().assertFormErrMsg("Users do not have permission to browse issue: " + BOB_USERNAME);

    	tester.setFormElement(FIELD_ID, ADMIN_USERNAME);
    	tester.submit();
    	assertions.forms().assertNoErrorsPresent();

    	assertWatchersPresent("TST-3", new String[]{ADMIN_USERNAME});
    	assertWatchersNotPresent("TST-3", new String[]{BOB_USERNAME});
    	
        addUserToGroup(BOB_USERNAME, JIRA_ADMIN_GROUP);

        params = new HashMap<>();
        params.put("security", new String[]{"10000"});
        params.put(FIELD_ID, new String[]{BOB_USERNAME, ADMIN_USERNAME});
        createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test add watchers ignoring permissions", params);

    	assertions.forms().assertNoErrorsPresent();
    	assertWatchersPresent("TST-4", new String[]{ADMIN_USERNAME, BOB_USERNAME});
    }
    
    /**
     * To test for issue #7, https://bitbucket.org/rbarham/jira-watcher-field-plugin/issue/7/null-pointer-exception-when-moving-issue
     */
	@Test
	@Restore(EXPORT_WITH_FIELD)
    public void testWorkflowTransition() {
    	String[] usernames = new String[]{ADMIN_USERNAME, BOB_USERNAME};

    	String issueKey = createIssue(PROJECT_ID, ISSUE_TYPE_BUG, "Test workflow transition.");
    	assertWatchersNotPresent(issueKey, usernames);
    }
}
