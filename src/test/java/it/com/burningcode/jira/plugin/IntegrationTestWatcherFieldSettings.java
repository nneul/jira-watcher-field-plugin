package it.com.burningcode.jira.plugin;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.functest.framework.Administration;
import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.assertions.Assertions;
import com.burningcode.jira.plugin.WatcherFieldSettings;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.FunctTestConstants.*;
import static it.com.burningcode.jira.IntegrationTestHelper.EXPORT_WITH_FIELD;

@LoginAs(user = "admin")
public class IntegrationTestWatcherFieldSettings extends BaseJiraFuncTest {
    @Inject
    public Administration administration;

    @Inject
    protected Assertions assertions;

    protected void assertBooleanSettingChange(final String setting, final boolean currentValue, final boolean newValue) {
        final String currentValueStr = String.valueOf(currentValue);
        final String newValueStr = String.valueOf(newValue);

        navigation.gotoResource("WatcherFieldSettings.jspa");

        tester.clickLink("edit_watcher_field_settings");

        tester.assertRadioOptionSelected(setting, currentValueStr);

        tester.setFormElement(setting, newValueStr);
        tester.assertRadioOptionSelected(setting, newValueStr);
        tester.submit("Update");
        navigation.gotoResource("EditWatcherFieldSettings!default.jspa");

        tester.assertRadioOptionSelected(setting, newValueStr);
    }

    /**
	 * Test to verify Issue #11 is fixed
	 */
    @Test
    @Restore(EXPORT_WITH_FIELD)
	public void testSettingsLink() {
		navigation.gotoAdmin();
		navigation.clickLinkWithExactText("Add-ons");
        assertions.getTextAssertions().assertTextPresent("Watcher Field Settings");
	}

    @Test
    @Restore(EXPORT_WITH_FIELD)
    public void testChangeSettings() {
        assertBooleanSettingChange(WatcherFieldSettings.ignoreUserPermissions, false, true);
        assertBooleanSettingChange(WatcherFieldSettings.ignoreWatcherPermissions, false, true);
        assertBooleanSettingChange(WatcherFieldSettings.ignoreDeactivatedWatchers, false, true);
        assertBooleanSettingChange(WatcherFieldSettings.automaticallyRemoveDeactivatedWatchers, false, true);
    }

    @Test
    @Restore(EXPORT_WITH_FIELD)
    public void testAccessWithoutPermissions() {
        administration.usersAndGroups().addUser(FRED_USERNAME, FRED_PASSWORD, FRED_FULLNAME, FRED_EMAIL, false);
        navigation.login(FRED_USERNAME);
        navigation.gotoResource("WatcherFieldSettings.jspa");
        assertions.getTextAssertions().assertTextPresent("Access Denied");
    }
}
