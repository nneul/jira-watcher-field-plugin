package it.com.burningcode.jira;

import java.util.HashMap;

@SuppressWarnings("WeakerAccess")
public class IntegrationTestHelper {
    public static final String PLUGIN_KEY = "com.burningcode.jira.issue.customfields.impl.jira-watcher-field";
    public static final String FIELD_TYPE_KEY = PLUGIN_KEY + ":watcherfieldtype";
    public static final String SEARCH_TYPE_KEY = PLUGIN_KEY + ":watcherfieldsearcher";

    public static final String NUMERIC_FIELD_ID = "10201";
    public static final String FIELD_ID = "customfield_" + NUMERIC_FIELD_ID;
    public static final String FIELD_NAME = "My Watchers";
    @SuppressWarnings("unused")
    public static final String FIELD_DESC = "Test watchers field.";
    public static final String FIELD_TYPE = "Watcher Field";
    @SuppressWarnings("unused")
    public static final String PROJECT_NAME = "Test";
    public static final String PROJECT_KEY = "TST";
    public static final String PROJECT_ID = "10000";

    public static final String EXPORT_WITH_FIELD = "JWF_FieldCreated.zip";
    public static final String EXPORT_WITHOUT_FIELD = "JWF_NoFieldCreated.zip";
    
    @SuppressWarnings("unused")
    public static HashMap<String, String[]> getUsernameFieldMap(String[] usernames) {
    	HashMap<String, String[]> params = new HashMap<>();
		params.put(FIELD_ID, usernames);
		return params;
	}
}
