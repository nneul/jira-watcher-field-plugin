package com.burningcode.jira.plugin;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.InvalidPropertyTypeException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * Used to handle settings for the JIRA Watcher Field.
 * @author Ray
 *
 */
@Scanned
public class WatcherFieldSettings extends JiraWebActionSupport {
	private static PropertySet propertySet;
	private static final long serialVersionUID = -8378909066515942570L;
	private static final Logger log = LoggerFactory.getLogger(WatcherFieldSettings.class);
	
	public static final String ignoreUserPermissions = "ignorePermissions";
	public static final String ignoreWatcherPermissions = "ignoreWatcherPermissions";
	public static final String ignoreDeactivatedWatchers = "ignoreDeactivatedWatchers";
	public static final String automaticallyRemoveDeactivatedWatchers = "automaticallyRemoveDeactivatedWatchers";

	private final JiraAuthenticationContext authenticationContext;
    private final GlobalPermissionManager globalPermissionManager;

	public WatcherFieldSettings(
			@ComponentImport GlobalPermissionManager globalPermissionManager,
			@ComponentImport JiraAuthenticationContext authenticationContext) {
		this.authenticationContext = authenticationContext;
        this.globalPermissionManager = globalPermissionManager;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		return super.doDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String doExecute() throws Exception {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		return super.doExecute();
	}

	/**
	 * Called when editing the settings
	 */
	public String doEdit() {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		PropertySet propertySet = getProperties();

		Map<?, ?> params = ActionContext.getParameters();

		setBooleanSetting(propertySet, params, ignoreUserPermissions);
		setBooleanSetting(propertySet, params, ignoreWatcherPermissions);
		setBooleanSetting(propertySet, params, ignoreDeactivatedWatchers);
		setBooleanSetting(propertySet, params, automaticallyRemoveDeactivatedWatchers);

		return getRedirect("WatcherFieldSettings.jspa");
	}

	public void setBooleanSetting(PropertySet propertySet, final Map<?, ?> params, final String setting) {
		if(params.containsKey(setting) && propertySet.isSettable(setting)) {
			Object paramValue = params.get(setting);
			if(paramValue instanceof String[] && ((String[])paramValue).length == 1) {
				propertySet.setBoolean(setting, Boolean.parseBoolean(((String[])paramValue)[0]));
			}
		}
	}

	/**
	 * Static method that returns the PropertySet used to get/store settings in the database
	 * @return The PropertySet to reference the data
	 */
	public static PropertySet getPropertySet() {
		if(propertySet == null) {
			HashMap<String, Object> args = new HashMap<>();
	        args.put("delegator.name", "default");
	        args.put("entityName", "WatcherFieldSettings");
	        args.put("entityId", (long) 1);

	        propertySet = PropertySetManager.getInstance("ofbiz", args);

			initBooleanSetting(ignoreUserPermissions, false);
			initBooleanSetting(ignoreWatcherPermissions, false);
			initBooleanSetting(ignoreDeactivatedWatchers, false);
			initBooleanSetting(automaticallyRemoveDeactivatedWatchers, false);
		}

		return propertySet;
	}

	protected static void initBooleanSetting(final String setting, final boolean defaultValue ) {
		try{
			// Set default settings
			if(!propertySet.exists(setting)) {
				propertySet.setBoolean(setting, defaultValue);
			}else{
				// Will throw an exception if of invalid type
				propertySet.getBoolean(setting);
			}
		}catch (InvalidPropertyTypeException e) {
			log.debug("Property ignorePermissions set to an invalid type.  Setting to default value, " + defaultValue + ".");
			propertySet.setBoolean(setting, defaultValue);
		}catch (Exception e) {
			log.debug("Error with ignorePermissions: "+e.getMessage());
			propertySet.setBoolean(setting, defaultValue);
		}
	}

    public static boolean ignoreBrowseIssuePermissions(){
        PropertySet propertySet = getPropertySet();
        return propertySet.exists(ignoreWatcherPermissions) && propertySet.getBoolean(ignoreWatcherPermissions);
    }

    public static boolean isIgnoreUserPermissions(ApplicationUser user) {
        PropertySet propertySet = getPropertySet();
        return propertySet.exists(ignoreUserPermissions) &&
                propertySet.getBoolean(ignoreUserPermissions) &&
                user == null;
    }

    public static boolean isIgnoreDeactivatedWatcher(@NotNull ApplicationUser user) {
        PropertySet propertySet = getPropertySet();
        return propertySet.exists(ignoreDeactivatedWatchers) &&
                propertySet.getBoolean(ignoreDeactivatedWatchers) &&
                !user.isActive();
    }

    public static boolean isAutomaticallyRemoveDeactivatedWatcher() {
        PropertySet propertySet = getPropertySet();
        return propertySet.exists(automaticallyRemoveDeactivatedWatchers) &&
                propertySet.getBoolean(automaticallyRemoveDeactivatedWatchers);
    }

	public boolean getIgnorePermissions() {
		return getPropertySet().getBoolean(ignoreUserPermissions);
	}

	public boolean getIgnoreWatcherPermissions() {
		return getPropertySet().getBoolean(ignoreWatcherPermissions);
	}

	public boolean getIgnoreDeactivatedWatchers() {
		return getPropertySet().getBoolean(ignoreDeactivatedWatchers);
	}

	public boolean getAutomaticallyRemoveDeactivatedWatchers() {
		return getPropertySet().getBoolean(automaticallyRemoveDeactivatedWatchers);
	}

	/**
	 * Method used to reference the {@link WatcherFieldSettings#getPropertySet()}
	 */
    private PropertySet getProperties() {
		return WatcherFieldSettings.getPropertySet();
	}

	public ApplicationUser getLoggedInUser() {
		return authenticationContext.getLoggedInUser();
	}
	
	/**
	 * Does the current logged in user has admin permissions
	 * @return True if has permissions, false otherwise.
	 */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
	private boolean hasAdminPermission() {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, getLoggedInUser());
    }
}
