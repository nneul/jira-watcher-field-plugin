package com.burningcode.jira.event.issue;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueWatcherAddedEvent;
import com.atlassian.jira.event.issue.IssueWatcherDeletedEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@ExportAsService
@Component
public class FixWatcherCounterListener implements InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(FixWatcherCounterListener.class);

    @ComponentImport
    private final EventPublisher eventPublisher;

    @ComponentImport
    private final WatcherManager watcherManager;

    @ComponentImport
    private final IssueManager issueManager;

    @ComponentImport
    private final JiraAuthenticationContext authenticationContext;

    @Inject
    public FixWatcherCounterListener(final EventPublisher eventPublisher, final WatcherManager watcherManager, final IssueManager issueManager, final JiraAuthenticationContext authenticationContext) {
        this.eventPublisher = eventPublisher;
        this.watcherManager = watcherManager;
        this.issueManager = issueManager;
        this.authenticationContext = authenticationContext;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @EventListener
    public void onWatcherAdded(IssueWatcherAddedEvent event) {
        fixWatcherCount(event.getIssue());
    }

    @EventListener
    public void onWatcherDeleted(IssueWatcherDeletedEvent event) {
        fixWatcherCount(event.getIssue());
    }

    private void fixWatcherCount(final Issue issue) {
        if(issue == null) {
            return;
        }

        final long actualWatcherCount = (long) watcherManager.getWatchers(issue, authenticationContext.getLocale()).size();

        if(issue.getWatches() == actualWatcherCount) {
            return;
        }

        MutableIssue issueObject = issueManager.getIssueObject(issue.getId());
        final long currentWatcherCount = issueObject.getWatches();

        if(currentWatcherCount == actualWatcherCount) {
            return;
        }

        log.warn("Watcher count for issue '" + issue.getKey() + "' is out of sync. " +
                 "Updating from '" + currentWatcherCount + "' to '" + actualWatcherCount + "'.");

        issueObject.setWatches(actualWatcherCount);
        issueObject.store();
    }
}
